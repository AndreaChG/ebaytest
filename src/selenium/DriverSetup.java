package selenium;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverSetup {

	static String pathToDriver = Paths.get("").toAbsolutePath().toString() +
			File.separator + "driverExecutables" + File.separator;
    
	public enum Browser {
        Chrome,
        IE,
        Firefox
    }
    
	public static WebDriver setupDriver(Browser browser) {
		WebDriver driver = null;
		String osName = (System.getProperty("os.name").toLowerCase().contains("mac") ? "mac" : "windows");
		if(browser == Browser.Chrome) {
			if (osName.equals("windows")) {
				System.setProperty("webdriver.chrome.driver", pathToDriver + "chromedriver.exe");
			} else {
				System.setProperty("webdriver.chrome.driver", pathToDriver + "chromedriver");
			}
			//ChromeOptions options = new ChromeOptions();
            //options.addArguments("headless");
            //options.addArguments("window-size=1200x600");
			driver = new ChromeDriver();
		}
		
		else if(browser == Browser.Firefox) {
			driver = new FirefoxDriver();
		}
		
		else if(browser == Browser.IE) {
			System.setProperty("webdriver.ie.driver", pathToDriver + "IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		return driver;
	}
	
}
