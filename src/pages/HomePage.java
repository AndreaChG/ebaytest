package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
	
	public HomePage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(id="gh-ac")
	private WebElement searchInputTextBox;
	
	@FindBy(id="gh-btn")
	private WebElement searchButton;

	public void searchProduct(String product) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		typeIn(searchInputTextBox, product);
		clickOn(searchButton);
	}
	
}
