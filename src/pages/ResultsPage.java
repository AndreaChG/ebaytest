package pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ResultsPage extends BasePage{
	
	public ResultsPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(css=".x-searchable-list__textbox__aspect-Brand")
	private WebElement searchBrandInputBox;
	
	@FindBy(css=".srp-controls__control.srp-controls__count")
	private WebElement results;
	
	@FindBy(css="#w5-w3 button")
	private WebElement sortDropdown;
	
	public WebElement getResults() {
		return results;
	}

	private WebElement checkBoxFilter(String brandName) {
		return driver.findElement(
				By.xpath(String.format("//input[@aria-label='%s']", brandName))
				);
	}
	
	private WebElement sortOptions(String sortOption) {
		return driver.findElement(
				By.xpath(String.format("//div[@class='srp-sort']//li//span[contains(text(),'%s')]", sortOption))
				);
	}
	
	public void selectBrand(String brandName) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		typeIn(wait.until(ExpectedConditions.visibilityOf(searchBrandInputBox)), brandName);
		clickOn(wait.until(ExpectedConditions.elementToBeClickable(checkBoxFilter(brandName))));
	}
	
	public void selecCondition(String condition) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		clickOn(wait.until(ExpectedConditions.elementToBeClickable(checkBoxFilter(condition))));
	}
	
	public void selectDeliveryOption(String deliveryOption) {
		clickOn(checkBoxFilter(deliveryOption));
	}
	
	public void sortBy(String sortOption) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		hoverOver(wait.until(ExpectedConditions.visibilityOf(sortDropdown)));
		clickOn(sortOptions(sortOption));
	}
	
	public static List<Double> getResultSearchProductPriceList() {
		WebElement priceList = driver.findElement(By.className("srp-river-results"));
		List<Double> prices = new ArrayList<Double>();
		for(int i=0; i<5; i++) {
			prices.add(Double.parseDouble(priceList.findElements(By.className("s-item__price")).get(i).getAttribute("textContent").substring(1)));
		}
		return prices;
    }
	
	
	public static List<String> getResultSearchProductNameList() {
		WebElement productList = driver.findElement(By.className("srp-river-results"));
		List<String> names = new ArrayList<String>();
		for(int i=0; i<5; i++) {
			names.add(productList.findElements(By.className("s-item__title")).get(i).getAttribute("textContent"));
		}
        return names;
    }
}
