package pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

public class ResultsFilteredPage extends ResultsPage{

	public ResultsFilteredPage(WebDriver driver) {
		super(driver);
	}
	
	public static Map<String, Double> getTopFiveProductsWithPrice() {
		Map<String, Double> map = new HashMap<>(); 
		List<Double> prices = getResultSearchProductPriceList();
		List<String> products = getResultSearchProductNameList();
		for (int i = 0; i<5; i++) {
			map.put(products.get(i).toString(), prices.get(i));
			
		}
		return map;
	}
	
	public void printTopFiveResults() {
		System.out.println("Top 5 List:");
		System.out.println("------------------------");
		for (int i = 0; i < getResultSearchProductPriceList().size(); i++) {
		    System.out.println("Product Name: " + getResultSearchProductNameList().get(i).toString()
		    				+ "\nPrice: " + getResultSearchProductPriceList().get(i).toString()
		    				+ "\n");
		}
	}
	
	public void printTopFivePrices() {
		for (int i = 0; i<5; i++) {
			System.out.println("Product Price["+ i +"]: " + getResultSearchProductPriceList().get(i).toString());
		}
	}
	
	public void printResults(){
		System.out.println("Search Results:");
		String[] splited = getResults().getText().split("\\s+");
		System.out.println(splited[0]);
		System.out.println("==========================");
	}


}

