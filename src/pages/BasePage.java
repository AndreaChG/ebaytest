package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;


public class BasePage extends PageFactory{
	
	protected static WebDriver driver;
	
	protected WebDriver getDriver() {
		return driver;
	}
	
	protected void scrollToElement(WebElement element) {
		((JavascriptExecutor) driver)
        .executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	protected void clickOn(WebElement element) {
		scrollToElement(element);
		element.click();
	}
	
	protected void typeIn(WebElement element, String text) {
		clickOn(element);
		element.clear();
		element.sendKeys(text);
	}
	
	protected String getTextOf(WebElement element) {
		return element.getText();
	}
	
	protected void hoverOver(WebElement element) {
		Actions hover = new Actions(getDriver());
		hover.moveToElement(element).build().perform();
	}
	
	public BasePage(final WebDriver driver) {
		BasePage.driver = driver;
		PageFactory.initElements(driver, this);
	}

}