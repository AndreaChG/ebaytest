package tests.ebayTest;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.ResultsFilteredPage;
import pages.ResultsPage;
import selenium.DriverSetup;


public class EbayTest{
	
	protected WebDriver driver;
	HomePage home;
	ResultsFilteredPage searchResults;
	
	@BeforeClass(alwaysRun = true)
	public void setupClass() throws IOException {
		try {   
			driver = DriverSetup.setupDriver(DriverSetup.Browser.Chrome);
		    driver.get("https://www.ebay.com");
		    driver.manage().window().maximize();
		    
        } catch (Exception e) {
            System.out.println("Error....." + e.getStackTrace());
        }
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setupTest()
	{
		home = new HomePage(driver);
		searchResults = new ResultsFilteredPage(driver);
	}

	@Test(priority = 1, description = "Filter by Brand and Condition")
	public void filter_by_brand_and_condition() throws Exception{
		home.searchProduct("shoes");
		searchResults.selectBrand("PUMA");
		searchResults.selecCondition("New without tags");
		searchResults.selectDeliveryOption("Free Shipping");
		searchResults.printResults();
	}
	
	@Test(priority = 2, description = "List the top 5 of the results list")
	public void list_the_top_5_of_the_results_list() throws Exception {
		searchResults.sortBy("Price + Shipping: lowest first");
		searchResults.printTopFiveResults();
	}
	
	@Test(priority = 3, description = "Verify Sort the first 5 results by price have sorted the items correctly")
	public void verify_the_first_5_sorted_by_price() throws Exception {
		
		List<Double> top5Prices = ResultsPage.getResultSearchProductPriceList();
		List<Double> sortedPrices = ResultsPage.getResultSearchProductPriceList();
		sortedPrices.sort(Comparator.naturalOrder());
		Assert.assertEquals(top5Prices,sortedPrices);
	}
	
	@Test(priority = 4, description = "Order and print the products by name in ascendant mode")
	public void order_and_print_the_products_by_name_ascendant() throws Exception {
		Map<String, Double> productList = ResultsFilteredPage.getTopFiveProductsWithPrice();
		LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<>();
		
		productList.entrySet()
	    .stream()
	    .sorted(Map.Entry.comparingByKey())
	    .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
		
		System.out.println("Sorted top 5 by product name ascendant: ");
		System.out.println("---------------------------------------");
		for (Map.Entry<String, Double> entry : sortedMap.entrySet())
		{
		    System.out.println("Product Name: " + entry.getKey() 
		    				+ "\nPrice: " + entry.getValue()
		    				+ "\n");
		}
	
	}
	
	@Test(priority = 5, description = "Order and print the products by price in descendant mode")
	public void order_and_print_the_products_by_price_descendant() throws Exception {
		Map<String, Double> productList = ResultsFilteredPage.getTopFiveProductsWithPrice();
		LinkedHashMap<String, Double> sortedMap = new LinkedHashMap<>();
		
		productList.entrySet()
	    .stream()
	    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
	    .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
		
		System.out.println("Sorted top 5 by product price descendant: ");
		System.out.println("---------------------------------------");
		for (Map.Entry<String, Double> entry : sortedMap.entrySet())
		{
		    System.out.println("Product Name: " + entry.getKey() 
		    				+ "\nPrice: " + entry.getValue()
		    				+ "\n");
		}
	
	}
	
	
	@AfterMethod(alwaysRun = true)
	public void tearDownTest(){

	}
	
	@AfterClass(alwaysRun = true)
	public void tearDownClass(){
		this.driver.close();
	}
	
}
